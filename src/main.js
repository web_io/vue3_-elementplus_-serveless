import { createApp } from 'vue'
import App from './App.vue'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import router from './router'
import store from './store'
const app = createApp(App)
const requireComponent = require.context(
    '@/components/',
    true,
    /.vue/
);
requireComponent.keys().forEach(fileName => {
    const componentConfig = requireComponent(fileName);
    const componentName = fileName.replace(/(\.\/|\.vue)/g, '').replace(new RegExp('/', 'g'), "");
    // 全局注册组件
    app.component(
        componentName,
        componentConfig.default || componentConfig
    );
    console.log('全局注册组件', app)
});
app.use(store);
app.use(router);
app.use(ElementPlus);
app.mount('#app');
