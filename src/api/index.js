import axios from "./http";
import{ resquest } from './base'
// ++++最新自定义接口++++
export function api_news_list(params) {
    return axios.post(`${resquest}/api_news_list`, params);
}
export function api_release_news(params) {
    return axios.post(`${resquest}/api_release_news`, params);
}
export function api_news_details(params) {
    return axios.post(`${resquest}/api_news_details`, params);
}