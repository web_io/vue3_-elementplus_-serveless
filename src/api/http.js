/****   http.js   ****/
// 导入封装好的axios实例
import axios from 'axios'

// 设置post请求头
axios.defaults.headers.post['Content-Type'] =
	'application/x-www-form-urlencoded'

const http ={
    /**
     * methods: 请求
     * @param url 请求地址 
     * @param params 请求参数
     */
    get(url,query){
        const config = {
            method: 'get',
            url:url,
            params:query
        }
        return axios(config)
    },
    post(url,params){
        const config = {
            method: 'post',
            url:url
        }
        if(params) config.data = params
        return axios(config)
    },
    put(url,params){
        const config = {
            method: 'put',
            url:url
        }
        if(params) config.params = params
        return axios(config)
    },
    delete(url,params){
        const config = {
            method: 'delete',
            url:url
        }
        if(params) config.params = params
        return axios(config)
    }
}
//导出
export default http