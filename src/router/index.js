import {createRouter, createWebHistory} from 'vue-router'
//router.js导入生成好的路由规则
import routerRoutes from "./routes";
const router = createRouter({
  history: createWebHistory(),
  routes: routerRoutes,//导入自动生成的所有路由规则
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition;
    } else {
      return {
        x: 0,
        y: 0
      };
    }
  },
});
//路由守卫，实现未登录组件跳转登陆页
router.beforeEach((to, form, next) => {
  //token
  const Token = localStorage.getItem('gbtoken');
  //当前路由
  const Rpath=to.path;
  //需要实现未登录跳转的路由集合
  let Rarray = [
    "/CurriculumList/player", 
    "/Uc/index", 
    "/SpecialList/view", 
  ];
  Rarray.forEach(item => {
    if (Rpath==item&&!Token) {
      router.push('/Sign_in/index')
    }
  });
  next();
});
export default router