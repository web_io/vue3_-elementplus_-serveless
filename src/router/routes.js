//routes.js
//递归获取src文件夹下的所有.vue文件
const files = require.context('@/', true, /.vue/)
let pages = {};
//生成路由规则
let generator = [];
files.keys().forEach(key => {
  pages[key.replace(/(\.\/|\.vue)/g, '')] = files(key).default;
});
//生成所有路由，并去除一级文件夹名称
Object.keys(pages).forEach(item => {
  let Rpath=item.replace(new RegExp('views/index','g'),"/").replace(new RegExp('views','g'),"").replace(new RegExp('components','g'),"");
  if (item!=='App') {
    generator.push({
        path: Rpath,
        name:Rpath,
        meta:{
          active:Rpath
        },
        component: pages[item],
    })
  }
  
});
//将生成路由导入合并
const routes = [
  ...generator,
];
console.log('路由',routes)
export default routes;
