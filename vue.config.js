/* 覆盖webpack的配置 */
module.exports = {
  devServer: { // 自定义服务配置
    open: true, // 自动打开浏览器
    port: 5566,
    proxy:{
      '/api_':{
          target: 'https://qc6vfb.api.cloudendpoint.cn',//代理地址，这里设置的地址会代替axios中设置的baseURL
          changeOrigin: true,// 如果接口跨域，需要进行这个参数配置
        }
    },
  },
}